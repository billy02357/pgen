MANPREFIX = /usr/share/man

pgen:
	$(CC) src/pgen.c -lsodium -o pgen
	clang-format -i src/*

install: pgen
	@mkdir -p $(DESTDIR)$(PREFIX)/usr/bin
	/bin/cp -vf pgen $(DESTDIR)$(PREFIX)/usr/bin
	chmod 755 $(DESTDIR)$(PREFIX)/usr/bin/pgen

clean:
	@rm -f pgen
