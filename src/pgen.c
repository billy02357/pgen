#include <sodium.h>
#include <string.h>
#include <unistd.h>

void print_help(void);

const char *all =
	"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!\"'$%&/()=?[]{}-_.:,;^*<>|@#~";
const char *letter = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
const char *alpha =
	"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";

const int default_length = 80;
char *default_type = "all";

void print_help(void)
{
	fprintf(stderr, "Usage: pgen [-l LENGTH] [-t TYPE]\n");
	fprintf(stderr,
		"Where LENGTH is the length of the generated password\n");
	fprintf(stderr,
		"and TYPE is all|letter|alpha; \"all\" meaning alphanumeric and symbols,\n");
	fprintf(stderr,
		"\"letter\" meaning lowercase and uppercase letters and \"alpha\" meaning\n");
	fprintf(stderr, "alphanumeric characters (letters and numbers)\n");
	fprintf(stderr, "Default length is 80 and default type is \"all\"\n");
	exit(1);
}

int main(int argc, char **argv)
{
	char *len_arg = NULL;
	char *type_arg = NULL;

	if (argc == 2 || argc == 3) {
		if (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h"))
			print_help();
	}


	int c;
	while ((c = getopt(argc, argv, "l:t:")) != -1) {
		switch (c) {
		case 't':
			type_arg = optarg;
			break;

		case 'l':
			len_arg = optarg;
			break;

		default:
			exit(1);
			break;
		}
	}

	int len;
	if (len_arg == NULL) {
		len = default_length;
	} else {
		len = atoi(len_arg);
	}

	char *s_type;
	if (type_arg == NULL) {
		s_type = default_type;
	} else {
		s_type = type_arg;
	}

	if (!strcmp(s_type, "all")) {
		for (int i = 0; i < len; i++) {
			int r = randombytes_uniform(strlen(all));
			printf("%c", all[r]);
		}
	} else if (!strcmp(s_type, "letter")) {
		for (int i = 0; i < len; i++) {
			int r = randombytes_uniform(strlen(letter));
			printf("%c", letter[r]);
		}
	} else if (!strcmp(s_type, "alpha")) {
		for (int i = 0; i < len; i++) {
			int r = randombytes_uniform(strlen(alpha));
			printf("%c", alpha[r]);
		}
	}


	printf("\n");
	return 0;
}
