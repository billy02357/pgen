# pgen
Password generator

# Dependencies
```
sudo pacman -S libsodium gcc clang
```

# Install
```
git clone https://codeberg.org/billy02357/pgen
cd pgen
sudo make install
```

# TODO
- [ ] Copy to clipboard with -c option (xclip)
